import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import {TypeOrmModule} from "@nestjs/typeorm";
import {Evento} from "./evento.entity";
import {Usuario} from "./usuario.entity";

@Module({
  imports: [TypeOrmModule.forRoot({
      type: 'postgres',
      //host: 'ec2-107-22-169-45.compute-1.amazonaws.com',
      host: 'localhost',
      port: 5432,
      //username: 'lgjdcbzecwmupg',
      username: 'postgres',
      //password: 'b1d372f7ce466cc07850d834e42df018aae7c96b5188a34340c31c785cc3ad88',
      password:'postgres',
      //database: 'dbbraj44grrcsk',
      database:'test',
      entities: [__dirname + '/../**/*.entity{.ts,.js}'],
      synchronize: true,
  }),TypeOrmModule.forFeature([Evento,Usuario])],
  controllers: [AppController],
  providers: [AppService]
})
export class AppModule {}
