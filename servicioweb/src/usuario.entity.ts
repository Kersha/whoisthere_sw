import {Column, PrimaryGeneratedColumn, ManyToMany, OneToMany, Entity} from "typeorm";
import {Evento} from "./evento.entity";


@Entity()
export class Usuario {
    @PrimaryGeneratedColumn()
    id: number;

    @Column('text')
    nombre:string;

    @Column('text')
    correo:string;

    @Column('text')
    password:string;

    @Column('text')
    fechaNa:string;

    @ManyToMany(type => Evento, evento => evento.usuarios)
    favoritos: Evento[];

    @OneToMany(type => Evento, evento=>evento.usuario)
    eventos:Evento[];
}