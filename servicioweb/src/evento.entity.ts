import {Entity, Column, PrimaryGeneratedColumn, ManyToOne, ManyToMany, JoinTable} from 'typeorm';
import {Usuario} from "./usuario.entity";

@Entity()
export class Evento{
    @PrimaryGeneratedColumn()
    id: number;

   @Column('text')
    nombre:string;

    @Column('text')
    fecha:string;

    @Column('text')
    horaInicio:string;

    @Column('text')
    horaFin:string;

    @Column('text')
    direccion:string;

    @Column('text')
    longitud:string;

    @Column('text')
    latitud:string;

    @Column('int')
    asistentes:number;

    @Column('text')
    descripcion:string;

    @ManyToOne(type => Usuario, usuario=>usuario.eventos)
    usuario:Usuario;

    @ManyToMany(type => Usuario, usuario => usuario.favoritos)
    @JoinTable()
    usuarios: Usuario[];
}