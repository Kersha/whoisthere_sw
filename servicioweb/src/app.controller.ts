import {Get, Controller, Post, Body, Query} from '@nestjs/common';
import { AppService } from './app.service';

@Controller()
export class AppController {
    constructor(private readonly appService: AppService) {}

    @Get()
    root(): string {
        return this.appService.root();
    }

    @Get('obtenerTodos')
    async obtenerEventos(){
        return await this.appService.obtenerTodos();
    }

    @Post('crear')
    async crearEvento(@Body('nombre') nombre,
                      @Body('fecha') fecha,
                      @Body('horaInicio') horaInicio,
                      @Body('horaFin') horaFin,
                      @Body('direccion') direccion ,
                      @Body('longitud') longitud,
                      @Body('latitud') latitud,
                      @Body('asistentes') asistentes,
                      @Body('descripcion') descripcion,
                      @Body('usuario')usuario)
    {
        return await this.appService.crearEvento(nombre, descripcion, asistentes,direccion,fecha,horaInicio,horaFin,usuario,latitud,longitud);
    }

    @Post('obtenerUno')
    async obtenerUno(@Body('idEvento')idEvento){
        return await this.appService.obtenerUno(idEvento);
    }

    @Post('obtenerPorUsuario')
    async  obtenerUsuario(@Body('usuario')usuario){
        return await this.appService.obtenerParaUsuario(usuario);
    }

    @Post('asistir')
    async asistir(@Body('idEvento')idEvento){
        return await  this.appService.avisarAsistencia(idEvento);
    }

    @Post('crearUsuario')
    async crearUsuario(@Body('nombre')nombre,@Body('correo')correo,@Body('password')password,@Body('fechaNac')fechaNac){
        return await this.appService.crearUsuario(nombre,correo,password,fechaNac);
    }

    @Post('favoritos')
    async obtenerFavoritos(@Body('idUsuario')idUsuario){
        return await this.appService.obtenerFavoritos(idUsuario);
    }

    @Post('borrarEvento')
    async borrarEvento(@Body('idEvento')idEvento){
        return await this.appService.borrarEvento(idEvento);
    }

    @Post('anadirFavoritos')
    async anadirFavoritos(@Body('idEvento')idEvento,@Body('idUsuario')idUsuario){
        return await this.appService.anadirFavoritos(idEvento,idUsuario);
    }

    @Post('quitarFavoritos')
    async quitarFavoritos(@Body('idEvento')idEvento,@Body('idUsuario')idUsuario) {
        return await this.appService.quitarFavoritos(idEvento, idUsuario);
    }

    @Post('obtenerIdUsuario')
    async obtenerid(@Body('nombre')nombre,@Body('password')password){
        return await  this.appService.obtenerUsuarioID(nombre,password);
    }
}
