import { Injectable } from '@nestjs/common';
import {InjectRepository} from "@nestjs/typeorm";
import {Repository} from "typeorm";
import {Evento} from "./evento.entity";
import {tryCatch} from "rxjs/internal/util/tryCatch";
import {Usuario} from "./usuario.entity";

@Injectable()
export class AppService {

  constructor( @InjectRepository(Evento)
               private readonly eventRepository: Repository<Evento>,
               @InjectRepository(Usuario)
               private readonly usuarioRepository: Repository<Usuario>){}

  root(): string {
    return 'Hello World!';
  }

  async crearEvento(nombre,descripcion,asistentes,direccion,fecha,horaInicio,horaFin,usuario,latitud,longitud){
      const evento= new Evento();
      evento.nombre=nombre;
      evento.descripcion=descripcion;
      evento.asistentes=asistentes;
      evento.direccion=direccion;
      evento.fecha=fecha;
      evento.horaInicio= horaInicio;
      evento.horaFin=horaFin;
      evento.usuario=await this.usuarioRepository.findOne(usuario);
      evento.latitud=latitud;
      evento.longitud=longitud;

      try{
          await this.eventRepository.save(evento);

          return {mensaje:"evento creado correctamente"}
      }catch (e) {
          return {mensaje:"Algo salio mal"}
      }
  }

  async obtenerTodos(){
      try{
          return await this.eventRepository.find();
      }catch (e) {
          return {mensaje:"Algo salio mal"}
      }
  }

  async obtenerUno(idEvento){
      try {
          return await this.eventRepository.findOne(idEvento);
      }catch (e) {
          return {mensaje:"no encontrado"}
      }

  }

  async avisarAsistencia(idEvento){
      let evento= await this.eventRepository.findOne(idEvento);
      evento.asistentes+=1;

      await this.eventRepository.save(evento);

      try {
          return evento;
      }catch (e) {
          return {mensaje:"Algo salio mal"}
      }

  }

  async obtenerParaUsuario(idUsuario){
      try {
          const usuario = await this.usuarioRepository.findOne(idUsuario, {relations: ["eventos"]});
          return usuario.eventos;
      }catch (e) {
          return {mensaje:"usuario no encontrado"}
      }

  }

  async crearUsuario(nombre,correo,password,fechaNac){
      try{
          const usuario=new Usuario();
          usuario.nombre=nombre;
          usuario.correo=correo;
          usuario.password=password;
          usuario.fechaNa=fechaNac;
          await this.usuarioRepository.save(usuario);
          return usuario;
      }catch (e) {
          return {mensaje:"algo salio mal"}
      }
  }

  async obtenerFavoritos(idUsuario){
      try{
          const usuario= await this.usuarioRepository.findOne(idUsuario,{relations:["favoritos"]});
          return usuario.favoritos
      }catch (e) {
          return{mensaje:"algo salio mal"}
      }
  }

  async borrarEvento(idEvento){
      try{
          const evento= await this.eventRepository.findOne(idEvento);
          this.eventRepository.delete(evento);
          return {mensaje:"evento borrado"}
      }catch (e) {
          return {mensaje:"algo salio mal"}
      }
  }

  async anadirFavoritos(idEvento,idUsuario){
      const evento= await this.eventRepository.findOne(idEvento);
      const usuario= await this.usuarioRepository.findOne(idUsuario,{relations:["favoritos"]});

      usuario.favoritos.push(evento);
      this.usuarioRepository.save(usuario);

      return usuario.favoritos;
  }

  async quitarFavoritos(idEvento,idUsuario){
      const evento= await this.eventRepository.findOne(idEvento);

      const usuario= await this.usuarioRepository.findOne(idUsuario,{relations:["favoritos"]});

      let index:any;
      usuario.favoritos.forEach((event,i)=>{
         if(event.id===evento.id){
             index=i;
         }
      });

      usuario.favoritos.splice(index,1);

      console.log("favoritasDESPUES",usuario.favoritos);
      this.usuarioRepository.save(usuario);

      return usuario.favoritos;
  }

  async obtenerUsuarioID(nombreUsuario,password){
      const usuario=await this.usuarioRepository.findOne({nombre:nombreUsuario,password:password});

      return usuario.id
  }
}
